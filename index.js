// Advance Query Operators
// We want more flexible querying of data within MongoDB

// [SECTION] Comparison Query Operators
/*
    $gt - greater than
    $gte - greater than equal

    Syntax:
        db.collectionName.find({field: {$gt/$gte: value}});
*/

db.users.find(
    {
        age:{$gt: 21}
    }
);

db.users.find(
    {
        age:{$gte: 21}
    }
);

/*
    $lt - less than
    $lte - less than equal

    Syntax:
        db.collectionName.find({field: {$lt/$lte: value}});
*/

db.users.find(
    {
        age: {
            $lt: 82
        }
    }
);

db.users.find(
    {
        age: {
            $lte: 82
        }
    }
);

db.users.updateMany(
    {department: "none"},
    {
        $set: {
            department: "HR"
		}
    }
);

// Not Equal
// $ne:

db.users.find({age: {$ne: 82}});

// $in
/*
    - Allows us to find documents with specific math criteria with one field using different values
    - db.collectionName.find(field:{$in: [valueA, valueB]});
*/

db.users.find(
    {
        lastName: {
            $in: ["Hawking", "Doe"]
        }
    }
);

db.users.find(
    {
        courses: {
            $in: ["HTML", "REACT"]
        }
    }
);


// $or
/*
    db.collectionName.find({$or[{fieldA:valueA}, {fieldB:valueB}]});
*/

db.users.find(
    {
        $or: [
            {firstName: "Neil"},
            {age: 25}
        ]
    }
);

// $and
/*
    db.collectionName.find({$and[{fieldA:valueA}, {fieldB:valueB}]});
*/

db.users.find(
    {
        $and: [
            {age: {$ne:82}},
            {age: {$ne:76}}
        ]
    }
);

// [SECTION] Field Projection
// To help with readability of the values returned, we can include/exclude fields from the response
/*
    Syntax:
    db.collectionName.find({criteria}, {fields you want to include, it should have a value of 1})
*/

db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }
);


// Exclusion
db.users.find(
    {
        firstName: "Jane"
    },
    {
        contact: 0,
        department: 0
    }
);

// $regex case sensitive

db.users.find(
    {
        firstName: {
            $regex: 'N'
        }
    }
);

// $regex not case sensitive
db.users.find(
    {
        firstName: {
            $regex: "n", $options: "$i"
        }
    }
);